import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Data.IORef
import Data.Time.Clock.POSIX

data State = State {
               deltaX      :: GLdouble
             , deltaY      :: GLdouble
             , moveSpeed   :: GLdouble
             , angle       :: GLdouble
             , rotateSpeed :: GLdouble
             , tstamp      :: GLdouble
             }

main = do
  _ <- getArgsAndInitialize
  initialDisplayMode $= [WithDepthBuffer]
  _ <- createWindow "Rotating Pyramid"
  now <- getTimestamp
  state <- newIORef State { deltaX = 0, deltaY = 0, moveSpeed = 0.5
                          , angle = 0.0, rotateSpeed = 0.2
                          , tstamp = now
                          }
  depthFunc $= Just Less
  displayCallback $= display state
  idleCallback $= Just (idle state)
  keyboardMouseCallback $= Just (keyboardMouse state)
  mainLoop

display state = do
  st <- get state
  clear [ColorBuffer, DepthBuffer]
  let rot  = rotateXZ (angle st)
      move = moveXY (deltaX st) (deltaY st)
      mr   = move.rot
      p1   = mr ( 0.58,-0.29,-0.00)
      p2   = mr (-0.29,-0.29,-0.50)
      p3   = mr (-0.29,-0.29, 0.50)
      p4   = mr ( 0.00, 0.58, 0.00)

  drawTriangle (Color4 1 0 0 1) p1 p2 p4
  drawTriangle (Color4 0 1 0 1) p1 p3 p4
  drawTriangle (Color4 0 0 1 1) p2 p3 p4
  drawTriangle (Color4 1 0 1 1) p1 p2 p3
  flush

idle state = do
  st <- get state
  now <- getTimestamp
  let dt  = now - tstamp st
      ms  = moveSpeed st
      dy' = deltaY st + ms * dt
      an  = angle st + rotateSpeed st * 2 * pi * dt
      an' = if an > 2*pi then an - 2*pi else an
      ms' = if abs dy' <= 0.5 then ms else -ms
  state $=! st { deltaY = dy', moveSpeed = ms'
               , angle = an', tstamp = now
               }
  postRedisplay Nothing

rotateXZ an (x, y, z) =
  let x' = (x * cos an) - (z * sin an)
      z' = (x * sin an) + (z * cos an)
   in (x', y, z')

moveXY dx dy (x, y, z) = 
  (x + dx, y + dy, z)

drawTriangle clr p1 p2 p3 = do
  let (p1x, p1y, p1z) = p1
      (p2x, p2y, p2z) = p2
      (p3x, p3y, p3z) = p3
  currentColor $= clr
  renderPrimitive Triangles $ do
    vertex $ Vertex3 p1x p1y p1z
    vertex $ Vertex3 p2x p2y p2z
    vertex $ Vertex3 p3x p3y p3z

keyboardMouse st key keyState _ {- modifiers -} _ {- pos -} =
  keyboardAct st key keyState

keyboardAct state (SpecialKey KeyLeft) Down = do
  st <- get state
  state $=! st { deltaX = deltaX st - 0.1 }

keyboardAct state (SpecialKey KeyRight) Down = do
  st <- get state
  state $=! st { deltaX = deltaX st + 0.1 }

keyboardAct _ _ _ =
  return ()

getTimestamp :: IO GLdouble
getTimestamp = do
  now <- getPOSIXTime
  return $ fromRational $ toRational now
