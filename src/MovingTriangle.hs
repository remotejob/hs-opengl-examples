import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Data.IORef
import Data.Time.Clock.POSIX

main = do
  _ <- getArgsAndInitialize
  _ <- createWindow "Moving Triangle"
  tstamp <- getTimestamp
  st <- newIORef (0.0, 0.0, 0.5, tstamp)
  keyboardMouseCallback $= Just (keyboardMouse st)
  displayCallback $= display st
  idleCallback $= Just (idle st)
  mainLoop

display st = do
  (dx, dy, _, _) <- get st
  clear [ColorBuffer]
  renderPrimitive Triangles $ do
    currentColor $= Color4 1 0 0 1
    vertex $ Vertex3 (dx + 0   :: GLdouble) (dy + 0.5) 0
    currentColor $= Color4 0 1 0 1
    vertex $ Vertex3 (dx + 0.5 :: GLdouble) (dy - 0.5) 0
    currentColor $= Color4 0 0 1 1
    vertex $ Vertex3 (dx - 0.5 :: GLdouble) (dy - 0.5) 0
  flush

idle st = do
  (dx, dy, delta, prevTStamp) <- get st
  tstamp <- getTimestamp
  let dt = tstamp - prevTStamp
      dy' = dy + delta * dt
      delta' = if abs dy' <= 0.5 then delta else -delta
  st $=! (dx, dy', delta', tstamp)
  postRedisplay Nothing

keyboardMouse st key keyState _ {-modifiers-} _ {- pos -} =
  keyboardAct st key keyState

keyboardAct st (SpecialKey KeyLeft) Down = do
  (dx, dy, delta, tstamp) <- get st
  let dx' = dx - 0.1
  st $=! (dx', dy, delta, tstamp)

keyboardAct st (SpecialKey KeyRight) Down = do
  (dx, dy, delta, tstamp) <- get st
  let dx' = dx + 0.1
  st $=! (dx', dy, delta, tstamp)

keyboardAct _ _ _ =
  return ()

getTimestamp :: IO GLdouble
getTimestamp = do
  now <- getPOSIXTime
  return $ fromRational $ toRational now
