import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT

main = do
  _ <- getArgsAndInitialize
  _ <- createWindow "Red Triangle"
  displayCallback $= display
  mainLoop

display = do
  clear [ColorBuffer]
  currentColor $= Color4 1 0 0 1
  renderPrimitive Triangles $ do
    vertex $ Vertex3 (0    :: GLfloat)   0.5  0
    vertex $ Vertex3 (0.5  :: GLfloat) (-0.5) 0
    vertex $ Vertex3 (-0.5 :: GLfloat) (-0.5) 0
  flush
