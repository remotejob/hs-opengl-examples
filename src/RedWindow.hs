import Graphics.UI.GLUT

main = do
  _ <- getArgsAndInitialize
  _ <- createWindow "Red Window"
  displayCallback $= display
  mainLoop

display = do
  clearColor $= Color4 1 0 0 1
  clear [ColorBuffer]
  flush
