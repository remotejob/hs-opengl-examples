import Graphics.UI.GLUT

main = do
  _ <- getArgsAndInitialize
  _ <- createWindow "Black Window"
  displayCallback $= display
  mainLoop

display = do
  clear [ColorBuffer]
  flush
