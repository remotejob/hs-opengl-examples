import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Graphics.GLUtil (readTexture)
import Control.Monad (forM_)
import Utils.Drawing (TexturedQuad(..), drawTexturedQuad)

main = do
  _ <- getArgsAndInitialize
  _ <- createWindow "Textured Walls"
  initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
  depthFunc          $= Just Less
  texture Texture2D  $= Enabled
  setupGeometry
  Right tex <- readTexture "./data/bricks.png"
  displayCallback    $= display tex
  mainLoop

setupGeometry :: IO ()
setupGeometry = do
   matrixMode  $= Projection
   perspective 50.0 1.0 0.3 10.0
   matrixMode  $= Modelview 0
   translate   $ Vector3 0.0 0.0 (-3.0 :: GLfloat)

display :: TextureObject -> IO ()
display tex = do
  clear [ColorBuffer, DepthBuffer]
  drawWalls tex
  swapBuffers

drawWalls :: TextureObject -> IO ()
drawWalls tex = do
  textureBinding Texture2D $= Just tex
  textureFilter  Texture2D $= ((Linear', Nothing), Linear')
  textureFunction          $= Replace
  forM_ [leftWall, frontWall, rightWall] drawTexturedQuad

leftWall =
  let (x1, x2, y1, y2, z1, z2) = (-0.9, -0.9, -0.6, 0.6, 0.6, -1.0)
      (tx1, tx2, ty1, ty2) = (3, 0, 1, 0) in
  wallTemplate x1 x2 y1 y2 z1 z2 tx1 tx2 ty1 ty2

frontWall =
  let (x1, x2, y1, y2, z1, z2) = (0.8, -0.8, -0.6, 0.6, -1.0, -1.0)
      (tx1, tx2, ty1, ty2) = (3, 0, 4, 0) in
  wallTemplate x1 x2 y1 y2 z1 z2 tx1 tx2 ty1 ty2

rightWall =
  let (x1, x2, y1, y2, z1, z2) = (0.9, 0.9, -0.6, 0.6, 0.6, -1.0)
      (tx1, tx2, ty1, ty2) = (0.9, 0.1, 0.9, 0.1) in
  wallTemplate x1 x2 y1 y2 z1 z2 tx1 tx2 ty1 ty2

wallTemplate x1 x2 y1 y2 z1 z2 tx1 tx2 ty1 ty2 =
  TexturedQuad x1 y1 z1 x1 y2 z1 x2 y2 z2 x2 y1 z2 tx1 tx2 ty1 ty2
