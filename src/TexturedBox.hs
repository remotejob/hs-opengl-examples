import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Graphics.GLUtil (readTexture)
import Control.Monad (forM_)
import Utils.Drawing (TexturedQuad(..), drawTexturedQuad)

main = do
  _ <- getArgsAndInitialize
  _ <- createWindow "Textured Box"
  initialDisplayMode $= [WithDepthBuffer, DoubleBuffered]
  depthFunc          $= Just Less
  texture Texture2D  $= Enabled
  setupGeometry
  Right tex <- readTexture "./data/box.jpg"
  displayCallback    $= display tex
  mainLoop

setupGeometry :: IO ()
setupGeometry = do
   matrixMode   $= Projection
   perspective  40.0 1.0 1.0 10.0
   matrixMode   $= Modelview 0
   translate    $ Vector3 0 0 (-5 :: GLfloat)
   rotate   30  $ Vector3 1 0 ( 0 :: GLfloat)
   rotate (-30) $ Vector3 0 1 ( 0 :: GLfloat)

display :: TextureObject -> IO ()
display tex = do
  clear [ColorBuffer, DepthBuffer]
  drawBox tex
  swapBuffers

drawBox :: TextureObject -> IO ()
drawBox tex = do
  textureBinding Texture2D $= Just tex
  textureFilter  Texture2D $= ((Linear', Nothing), Linear')
  textureFunction          $= Replace
  forM_ [boxFront, boxBack, boxTop, boxBottom, boxLeft, boxRight]
    drawTexturedQuad

boxFront  = faceTemplate   ( 1, -1,  1, -1,  1,  1)
boxBack   = faceTemplate   ( 1, -1,  1, -1, -1, -1)
boxLeft   = faceTemplate   (-1, -1,  1, -1,  1, -1)
boxRight  = faceTemplate   ( 1,  1,  1, -1,  1, -1)
boxTop    = faceTemplateXZ ( 1, -1,  1,  1, -1)
boxBottom = faceTemplateXZ ( 1, -1, -1,  1, -1)
 
faceTemplate (x1, x2, y1, y2, z1, z2) =
  TexturedQuad x1 y1 z1 x1 y2 z1 x2 y2 z2 x2 y1 z2 0 1 0 1

faceTemplateXZ (x1, x2, y, z1, z2) =
  TexturedQuad x1 y z1 x1 y z2 x2 y z2 x2 y z1 0 1 0 1
